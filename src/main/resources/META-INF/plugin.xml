<!-- Plugin Configuration File. Read more: https://plugins.jetbrains.com/docs/intellij/plugin-configuration-file.html -->
<idea-plugin>
    <!-- Unique identifier of the plugin. It should be FQN. It cannot be changed between the plugin versions. -->
    <id>se.wellfish.Code-file-grabber</id>
    <!-- Public plugin name should be written in Title Case.
         Guidelines: https://plugins.jetbrains.com/docs/marketplace/plugin-overview-page.html#plugin-name -->
    <name>Code-File-Grabber</name>

    <!-- A displayed Vendor name or Organization ID displayed on the Plugins Page. -->
    <vendor email="admin@wellfish.se" url="https://wellfish.se">Wellfish AB</vendor>

    <!-- Description of the plugin displayed on the Plugin Page and IDE Plugin Manager.
         Simple HTML elements (text formatting, paragraphs, and lists) can be added inside of <![CDATA[ ]]> tag.
         Guidelines: https://plugins.jetbrains.com/docs/marketplace/plugin-overview-page.html#plugin-description -->
    <description>
        <![CDATA[
            CodeFileGrabber is a convenient plugin developed for IntelliJ IDEA and JetBrains products, enhancing the user experience by appending filenames to copied code, offering a richer context.
            The benefits are multiple, especially useful when interaction with Large Language Models (LLM) or during collaborative efforts requiring precise code references. The primary features are:
            <ul>
              <li>Seamless integration into IDEA's context menu.</li>
              <li>Automatic appending of filenames to copied code snippets.</li>
              <li>Enhanced clarity and traceability in shared code segments.</li>
            </ul>
            For those who seek to maintain clarity and ensure effective communication through code, CodeFileGrabber is an apt choice.
        ]]>
    </description>
    <change-notes><![CDATA[
        <p>Support for 2025.1</p>
    ]]></change-notes>

    <!-- Product and plugin compatibility requirements.
         Read more: https://plugins.jetbrains.com/docs/intellij/plugin-compatibility.html -->
    <depends>com.intellij.modules.platform</depends>

    <!-- Extension points defined by the plugin.
         Read more: https://plugins.jetbrains.com/docs/intellij/plugin-extension-points.html -->
    <extensions defaultExtensionNs="com.intellij">

    </extensions>
    <actions>
        <group
                id="CodeHeaderGroup"
                class="se.wellfish.codefilegrabber.CodeHeaderGroup"
                text="Static Grouped Actions"
        >
            <add-to-group
                    group-id="EditorPopupMenu"
                    anchor="after"
                    relative-to-action="$Copy"/>
            <action
                    id="CodeHeaderAction2"
                    class="se.wellfish.codefilegrabber.CodeHeaderAction"
                    text="Copy File and Filename"
                    description="SDK action example"
                    icon="AllIcons.Actions.InlayRenameInNoCodeFiles"
            >
            </action>
            <action id="CodeHeaderSelectionAction"
                    class="se.wellfish.codefilegrabber.CodeHeaderSelectionAction"
                    text="Copy Selection and Filename"
                    description="Get text selection and prepend filename as comment header"
                    icon="AllIcons.Actions.InlayRenameInNoCodeFiles"
            >
                <keyboard-shortcut
                        keymap="$default"
                        first-keystroke="control alt C"
                />
            </action>
        </group>


        <action id="CodeHeaderFilesAction"
                class="se.wellfish.codefilegrabber.CodeHeaderFilesAction"
                text="Copy Files Content and Filename"
                description="Get one or multiple files code with filenames as 'header'"
                icon="AllIcons.Actions.InlayRenameInNoCodeFiles"
        >
            <add-to-group group-id="CutCopyPasteGroup" anchor="after" relative-to-action="$Copy"/>
            <keyboard-shortcut
                    keymap="$default"
                    first-keystroke="control alt C"
            />
        </action>
    </actions>
    <extensions defaultExtensionNs="com.intellij">
        <applicationConfigurable
                parentId="tools"
                instance="se.wellfish.codefilegrabber.PluginSettingsConfigurable"
                id="se.wellfish.codefilegrabber.PluginSettingsConfigurable"
                displayName="Code File Grabber"/>

        <applicationService
                serviceImplementation="se.wellfish.codefilegrabber.PluginSettingsState"/>
    </extensions>
</idea-plugin>
