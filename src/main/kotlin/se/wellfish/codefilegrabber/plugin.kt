package se.wellfish.codefilegrabber

import com.intellij.openapi.actionSystem.*
import com.intellij.openapi.editor.Document
import com.intellij.openapi.fileEditor.FileDocumentManager
import com.intellij.openapi.util.TextRange
import com.intellij.openapi.util.text.StringUtil
import com.intellij.openapi.vfs.LocalFileSystem
import com.intellij.openapi.vfs.VfsUtil
import org.jetbrains.annotations.NotNull
import java.awt.Toolkit
import java.awt.datatransfer.StringSelection

/**
 * This is the action that is used to copy a whole file in the editor.
 */
class CodeHeaderAction : AnAction() {
    override fun actionPerformed(e: AnActionEvent) {
        // Get all the required data from data keys
        // Replace deprecated getRequiredData with getData and null check
        val editor = e.getData(CommonDataKeys.EDITOR) ?: return
        e.getData(CommonDataKeys.PROJECT) ?: return

        val document = editor.document
        val comment = getCodeComment(document, e)

        val code = document.text
        val fullCode = StringUtil.join(comment, code)

        val stringSelection = StringSelection(fullCode)
        val clipboard = Toolkit.getDefaultToolkit().systemClipboard
        clipboard.setContents(stringSelection, null)
    }

    /**
     * Sets visibility and enables this action menu item if:
     *
     *  * a project is open
     *  * an editor is active
     *
     * @param e Event related to this action
     */
    override fun update(e: AnActionEvent) {
        // Get required data keys
        val project = e.project
        val editor = e.getData(CommonDataKeys.EDITOR)
        // Set visibility and enable only in case of existing project and editor and if a selection exists
        e.presentation.isEnabledAndVisible = project != null && editor != null
    }

    override fun getActionUpdateThread(): ActionUpdateThread {
        return ActionUpdateThread.BGT
    }
}

/**
 * This is the action that is used when copy text in the editor.
 */
class CodeHeaderSelectionAction : AnAction() {
    override fun actionPerformed(@NotNull e: AnActionEvent) {
        // Get all the required data from data keys
        // Replace deprecated getRequiredData with getData and null check
        val editor = e.getData(CommonDataKeys.EDITOR) ?: return
        e.getData(CommonDataKeys.PROJECT) ?: return

        val document = editor.document
        // Work off of the primary caret to get the selection info
        val primaryCaret = editor.caretModel.primaryCaret
        val start = primaryCaret.selectionStart
        val end = primaryCaret.selectionEnd

        // Get the selected text
        val code = document.getText(TextRange(start, end))

        val comment = getCodeComment(document, e)
        val fullCode = StringUtil.join(comment, code)
        val stringSelection = StringSelection(fullCode)

        val clipboard = Toolkit.getDefaultToolkit().systemClipboard
        clipboard.setContents(stringSelection, null)
    }

    /**
     * Sets visibility and enables this action menu item if:
     *
     *  * a project is open
     *  * an editor is active
     *  * some characters are selected
     *
     *
     * @param e Event related to this action
     */
    override fun update(e: AnActionEvent) {
        // Get required data keys
        val project = e.project
        val editor = e.getData(CommonDataKeys.EDITOR)
        // Set visibility and enable only in case of existing project and editor and if a selection exists
        e.presentation.isEnabledAndVisible = project != null && editor != null && editor.selectionModel.hasSelection()
    }

    override fun getActionUpdateThread(): ActionUpdateThread {
        return ActionUpdateThread.BGT
    }
}

/**
 * This is the action that is used when copy files in the project view.
 */
class CodeHeaderFilesAction : AnAction() {
    override fun actionPerformed(e: AnActionEvent) {
        e.project ?: return

        val files = e.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY) ?: return

        val code = StringBuilder()
        for (file in files) {
            if (file.isDirectory) {
                continue
            }

            val document = FileDocumentManager.getInstance().getDocument(file) ?: return
            val comment = getCodeComment(document, e)
            val fileCode = document.text
            code.append(StringUtil.join(comment, fileCode, "\n"))
        }

        val stringSelection = StringSelection(code.toString())
        val clipboard = Toolkit.getDefaultToolkit().systemClipboard
        clipboard.setContents(stringSelection, null)
    }

    override fun update(e: AnActionEvent) {
        val files = e.getData(CommonDataKeys.VIRTUAL_FILE_ARRAY)
        e.presentation.isEnabledAndVisible = !files.isNullOrEmpty()
    }

    override fun getActionUpdateThread(): ActionUpdateThread {
        return ActionUpdateThread.BGT
    }
}

class CodeHeaderGroup : DefaultActionGroup() {
    override fun update(e: AnActionEvent) {
        val project = e.project
        val editor = e.getData(CommonDataKeys.EDITOR)
        // Set visibility and enable only in case of existing project and editor and if a selection exists
        e.presentation.isEnabledAndVisible = project != null && editor != null
    }

    override fun getActionUpdateThread(): ActionUpdateThread {
        return ActionUpdateThread.BGT
    }
}

/***
 * Get the comment to be added to the copied code. i.e. the filename or the filepath.
 */
private fun getCodeComment(
    document: Document,
    e: AnActionEvent
): String {
    val file = FileDocumentManager.getInstance().getFile(document) ?: return ""

    val settings = PluginSettingsState.getInstance()
    val projectBaseDir = e.project?.basePath ?: return ""
    val projectBaseDirFile = LocalFileSystem.getInstance().findFileByPath(projectBaseDir) ?: return ""
    val relativePath = VfsUtil.getRelativePath(file, projectBaseDirFile)
    val filename = if (settings.useFilePath && relativePath != null) relativePath else file.name
    val comment = "// $filename\n"
    return comment
}
