package se.wellfish.codefilegrabber

import com.intellij.openapi.options.Configurable
import com.intellij.ui.components.JBLabel
import com.intellij.util.ui.FormBuilder
import javax.swing.ButtonGroup
import javax.swing.JComponent
import javax.swing.JPanel
import javax.swing.JRadioButton

class PluginSettingsConfigurable : Configurable {
    private var mySettingsComponent: PluginSettingsComponent? = null

    override fun createComponent(): JComponent {
        mySettingsComponent = PluginSettingsComponent()
        return mySettingsComponent!!.getPanel()
    }

    override fun isModified(): Boolean {
        val settings = PluginSettingsState.getInstance()
        val modifiedFilePath = mySettingsComponent!!.getUseFilePath() != settings.useFilePath
        val modifiedFileName = mySettingsComponent!!.getUseFileName() != settings.useFileName
        return modifiedFilePath || modifiedFileName
    }

    override fun apply() {
        val settings = PluginSettingsState.getInstance()
        settings.useFilePath = mySettingsComponent!!.getUseFilePath()
        settings.useFileName = mySettingsComponent!!.getUseFileName()
    }

    override fun reset() {
        val settings = PluginSettingsState.getInstance()
        mySettingsComponent!!.setUseFilePath(settings.useFilePath)
        mySettingsComponent!!.setUseFileName(settings.useFileName)
    }

    override fun disposeUIResources() {
        mySettingsComponent = null
    }

    override fun getDisplayName(): String {
        return "Code File Grabber"
    }
}

class PluginSettingsComponent {
    private var mainPanel = JPanel()
    private val useFilePathRadioButton = JRadioButton("Use file path")
    private val useFileNameRadioButton = JRadioButton("Use file name")
    private val buttonGroup = ButtonGroup()

    init {
        // Add the radio buttons to a group to ensure only one can be selected at a time
        buttonGroup.add(useFilePathRadioButton)
        buttonGroup.add(useFileNameRadioButton)

        mainPanel = FormBuilder.createFormBuilder()
            .addLabeledComponent(JBLabel("Append filename or filepath"), useFilePathRadioButton, 1, false)
            .addLabeledComponent(JBLabel(""), useFileNameRadioButton, 1, false)
            .panel
    }

    fun getPanel(): JPanel {
        return mainPanel
    }

    fun getUseFilePath(): Boolean {
        return useFilePathRadioButton.isSelected
    }

    fun getUseFileName(): Boolean {
        return useFileNameRadioButton.isSelected
    }

    fun setUseFilePath(useFilePath: Boolean) {
        useFilePathRadioButton.isSelected = useFilePath
    }

    fun setUseFileName(useFileName: Boolean) {
        useFileNameRadioButton.isSelected = useFileName
    }
}
