package se.wellfish.codefilegrabber

import com.intellij.openapi.components.*

@State(name = "se.wellfish.codefilegrabber.PluginSettingsState", storages = [Storage("SdkSettingsPlugin.xml")])
class PluginSettingsState : PersistentStateComponent<PluginSettingsState> {
    var useFilePath: Boolean = false
    var useFileName: Boolean = false

    override fun getState(): PluginSettingsState {
        return this
    }

    override fun loadState(state: PluginSettingsState) {
        useFilePath = state.useFilePath
        useFileName = state.useFileName
    }

    companion object {
        fun getInstance(): PluginSettingsState {
            return service()
        }
    }
}
